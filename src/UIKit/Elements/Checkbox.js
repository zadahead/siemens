import './Checkbox.css';
import { Icon } from 'UIKit';

const Checkbox = (props) => {
    return (
        <div
            className={`Checkbox${props.selected ? ' selected' : ''}`}
            onClick={props.onClick}
        >
            <Icon i={props.selected ? 'check-square' : 'square'} />
        </div>
    )
}

export default Checkbox;