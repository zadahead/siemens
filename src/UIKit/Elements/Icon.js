import './Icon.css';

const Icon = (props) => {
    return (
        <div className={`Icon${props.onClick ? ' clickable' : ''}`}>
            <i className={`far fa-${props.i}`} onClick={props.onClick}></i>
        </div>
    )
}

export default Icon;