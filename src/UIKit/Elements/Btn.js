import { Icon, Line } from "UIKit";
import './Btn.css';

const Btn = (props) => {
    return (
        <button className="Btn" onClick={props.onClick}>
            <Line justify="between">
                <div>{props.children}</div>
                {props.i && <Icon i={props.i} />}
            </Line>
        </button>
    )
}

export default Btn;