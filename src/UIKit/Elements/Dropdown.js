import { useRef, useEffect, useState } from 'react';
import { Icon, Line } from 'UIKit';
import './Dropdown.css';

const Dropdown = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const wrapper = useRef();

    useEffect(() => {
        document.addEventListener('click', handleBodyClick);

        return () => {
            document.removeEventListener('click', handleBodyClick);
        }
    }, [])

    const handleIsOpen = () => {
        setIsOpen(!isOpen);
    }

    const handleSelect = (item) => {
        props.onChange(item.id);
        setIsOpen(false);
    }

    const handleBodyClick = (e) => {
        console.log('triggered!!');

        if (!wrapper.current) { return; }

        if (!wrapper.current.contains(e.target)) {
            setIsOpen(false);
        }
    }

    const renderList = () => {
        return props.list.map(i => {
            return <li key={i.id} onClick={() => handleSelect(i)}>{i.value}</li>;
        })
    }

    const renderTitle = () => {
        if (props.selected) {
            return props.list.find(i => i.id === props.selected).value;
        }
        return 'please select';
    }

    return (
        <div className="Dropdown" ref={wrapper}>
            <div className="trigger" onClick={handleIsOpen}>
                <Line>
                    <div>{renderTitle()}</div>
                    <Icon i="chevron-down" />
                </Line>
            </div>
            {isOpen &&
                <ul className="list">
                    {renderList()}
                </ul>
            }
        </div>
    )
}

export default Dropdown;