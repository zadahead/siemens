import { useSelector } from 'react-redux';


const CounterDisplay = () => {
    const store = useSelector(state => state);
    console.log('store', store);

    return (
        <div>
            <h1>Count: {store.count}</h1>
        </div>
    )
}

export default CounterDisplay;