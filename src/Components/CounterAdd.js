import { useDispatch } from 'react-redux';
import { addCount } from 'State/Counter';

const CounterAdd = () => {
    const dispatch = useDispatch();

    return (
        <div>
            <button onClick={() => dispatch(addCount(5))}>Add Count</button>
        </div>
    )
}

export default CounterAdd;