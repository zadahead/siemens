const Box = (props) => {
    console.log('props', props);

    return (
        <div>
            <h1>{props.title}</h1>
            <h2>*******************</h2>
            <h3>
                {props.children}
            </h3>
            <h2>*******************</h2>
        </div>
    )
}

export default Box;