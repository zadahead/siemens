const Users = (props) => {
    console.log(props);

    const renderList = () => {
        return props.list.map((i) => {
            return <h2 key={i.id}>{i.name}</h2>
        })
    }

    return (
        <div>
            <h1>Users List:</h1>
            {renderList()}
        </div>
    )
}

export default Users;