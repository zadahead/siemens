import axios from "axios";

//actions 
export const addCount = (amount = 1) => {
    return (dispatch) => {
        dispatch({
            type: 'ADD_COUNT',
            payload: amount
        });
    }
}

//reducer 
export const counterReducer = (state = 0, action) => {
    switch (action.type) {
        case 'ADD_COUNT': return state + action.payload;
        default: return state;
    }
}
