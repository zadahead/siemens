import axios from "axios";

export const getTodos = () => {
    return async (dispatch) => {
        const resp = await axios.get('https://jsonplaceholder.typicode.com/todos');
        dispatch({
            type: 'GET_TODOS',
            payload: resp.data.slice(0, 10)
        });
    }
}

export const patchTodos = (id) => {
    return {
        type: 'PATCH_TODOS',
        payload: id
    }
}


export const deleteTodos = (id) => {
    return {
        type: 'DELETE_TODOS',
        payload: id
    }
}

export const postTodos = (title) => {
    return {
        type: 'POST_TODOS',
        payload: title
    }
}

//

export const todosReducer = (state = [], action) => {
    switch (action.type) {
        case 'GET_TODOS': return action.payload;
        case 'PATCH_TODOS':
            let clone = [...state];
            let index = clone.findIndex(i => i.id === action.payload);
            clone[index].completed = !clone[index].completed;
            return clone;
        case 'DELETE_TODOS':
            let deleteClone = [...state];
            let deleteIndex = deleteClone.findIndex(i => i.id === action.payload);
            deleteClone.splice(deleteIndex, 1);
            return deleteClone;
        case 'POST_TODOS':
            let postClone = [...state];
            postClone.push({
                id: Math.floor(Math.random() * 10000),
                title: action.payload,
                completed: false
            })
            return postClone;
        default: return state;
    }
}