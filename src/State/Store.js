import { createStore, combineReducers, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";

import { counterReducer } from "./Counter";
import { todosReducer } from "./Todos";

const middlewares = applyMiddleware(ReduxThunk);

const reducers = combineReducers({
    count: counterReducer,
    todos: todosReducer
})

const store = createStore(reducers, middlewares);

export default store;