
import './App.css';

import { Icon, Grid, Line } from "UIKit";

import { Route, NavLink, Switch, Redirect } from 'react-router-dom';

import Users from 'Pages/Users';
import Todos from 'Pages/Todos';
import User from 'Pages/User';
import Hooks from 'Pages/Hooks';
import Api from 'Pages/Api';
import Redux from 'Pages/Redux';


const App = () => {

    return (
        <div className="App">
            <Grid>
                <div>
                    <Line justify="between">
                        <div>
                            <Line>
                                <Icon i="heartbeat" />
                            </Line>
                        </div>
                        <Line>
                            <NavLink to="/redux">Redux</NavLink>
                            <NavLink to="/api">Api</NavLink>
                            <NavLink to="/todos">Todos</NavLink>
                            <NavLink to="/hooks">Hooks</NavLink>
                        </Line>
                    </Line>
                </div>
                <div>
                    <Switch>
                        <Route exact path="/users/:type/:id" component={User}></Route>
                        <Route exact path="/users" component={Users}></Route>
                        <Route exact path="/redux" component={Redux}></Route>
                        <Route path="/todos" component={Todos}></Route>
                        <Route path="/hooks" component={Hooks}></Route>
                        <Route path="/api" component={Api}></Route>

                        <Redirect to='/todos' />
                    </Switch>
                </div>
            </Grid>
        </div>
    )
}

export default App;