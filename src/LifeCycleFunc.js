//mounted - (rendered)
//update
//unmounted - (removed)

import { useEffect, useState } from "react";

const LifeCycleFunc = () => {
    const [count, setCount] = useState(0);
    const [color, setColor] = useState('red');

    const handleCount = () => {
        setCount(count + 1);
    }

    const handleChangeColor = () => {
        setColor(color === 'red' ? 'blue' : 'red');
    }

    console.log('RENDER');

    useEffect(() => {
        //console.log('mounted (componentDidMount)');

        return () => {
            //console.log('unmounted (componentWillUnmount)');
        }
    }, []);

    useEffect(() => {
        //console.log('did update (componentDidUpdate)', count);

        return () => {
            //console.log('item unmounted (componentWillUpdate)', count);
        }
    })

    useEffect(() => {
        console.log('color mounted / updated', color);

        return () => {
            console.log('color unmounted (componentWillUpdate)', color);
        }
    }, [color])

    return (
        <div>
            <h1 onClick={handleCount}>LifeCycleFunc count, {count}</h1>
            <h1 onClick={handleChangeColor}>LifeCycleFunc color, {color}</h1>
        </div>
    )
}

export default LifeCycleFunc;