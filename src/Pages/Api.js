import { useState, useEffect } from "react";
import axios from "axios";

const useFetch = (url) => {
    //state - list
    const [list, setList] = useState(null);

    //mount - fetch API
    useEffect(() => {
        axios.get(url)
            .then(resp => {
                setList(resp.data);
            })
    }, [])

    return list;
}

const Api = () => {
    const list = useFetch('https://jsonplaceholder.typicode.com/todos');

    const renderList = () => {
        if (list) {
            return list.map(i => {
                const styleCss = {
                    color: i.completed ? 'gray' : ''
                };

                return <h2 key={i.id} style={styleCss}>{i.title}</h2>
            })
        }
    }
    //loading..

    return (
        <div>
            <h1>API!</h1>
            {!list && <h2>loading...</h2>}
            {renderList()}
        </div>
    )
}

//useFetch(url)

export default Api;