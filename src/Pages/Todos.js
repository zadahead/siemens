import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react/cjs/react.development';
import { Btn, Checkbox, Icon, Input, Line, Rows } from 'UIKit';

import { getTodos, patchTodos, deleteTodos, postTodos } from 'State/Todos';
import { useInput } from './Hooks';

const Todos = () => {
    const newTodo = useInput('');
    const list = useSelector(state => state.todos);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getTodos());
    }, [])

    const handleCompleted = (item) => {
        dispatch(patchTodos(item.id));
    }

    const handleDelete = (item) => {
        dispatch(deleteTodos(item.id))
    }

    const handleAdd = () => {
        dispatch(postTodos(newTodo.value))
    }

    return (
        <Rows>
            <h1>Todos</h1>
            <h2>
                This is great
            </h2>
            {!list.length && <h2>loading...</h2>}
            <Rows>
                {list.length &&
                    list.map(i => (
                        <Line key={i.id} justify="between">
                            <Line>
                                <Checkbox selected={i.completed} onClick={() => handleCompleted(i)} />
                                <div style={{ color: i.completed ? '#e1e1e1' : '' }}>{i.title}</div>
                            </Line>
                            <Icon i="trash" onClick={() => handleDelete(i)} />
                        </Line>
                    ))
                }
            </Rows>
            <Line >
                <Input {...newTodo} />
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </Rows>
    )
}

export default Todos;