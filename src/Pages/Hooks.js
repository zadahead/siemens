import { useState } from "react";

export const useInput = (initValue, placeholder) => {
    const [value, setValue] = useState(initValue);

    const onChange = (e) => {
        setValue(e.target.value);
    }

    return {
        value,
        onChange,
        placeholder
    }
}

const Hooks = () => {
    const username = useInput('', 'username');
    const password = useInput('', 'password');

    const handleSubmit = () => {
        console.log(username.value, password.value);
    }

    return (
        <div>
            <input {...username} />
            <input {...password} />
            <button onClick={handleSubmit}>Show Value</button>
        </div>
    )
}

export default Hooks;