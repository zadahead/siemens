import CounterAdd from "Components/CounterAdd";
import CounterDisplay from "Components/CounterDisplay";

const Redux = () => {

    return (
        <div>
            <CounterAdd />
            <CounterDisplay />
        </div>
    )
}

export default Redux;