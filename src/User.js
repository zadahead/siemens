import { useState } from "react";

export const User = (props) => {
    return (
        <div>
            <span>{props.fname}</span>
            {props.isLname && <span>{` ${props.lname}`}</span>}
            <button onClick={props.handleSetFname}>Remove fname</button>
        </div>
    )
}

export const TitleUser = (props) => {
    return (
        <div>
            <h1>This is a Title</h1>
            <User {...props} />
        </div>
    )
}

export default User;