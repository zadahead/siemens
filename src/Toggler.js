import { useState } from 'react';
import User from './User';

const Toggler = () => {
    const [isShown, setIsShown] = useState(true);
    const [isLname, setIsLname] = useState(true);

    const handleToggle = () => {
        setIsShown(!isShown);
    }

    const handleSetFname = () => {
        setIsLname(!isLname);
    }

    const renderShown = () => {
        if (isShown) {
            return <User fname="asdasd" lname="aads" isLname={isLname} handleSetFname={handleSetFname} />
        }
        return null;
    }
    return (
        <div>
            <h1>Toggler</h1>
            <button onClick={handleToggle}>Toggle</button>
            {renderShown()}
        </div>
    )
}

export default Toggler;