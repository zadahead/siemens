import React from "react";

//mounted - (rendered)
//update 
//unmounted - (removed)

class LifeCycleClass extends React.Component {
    state = {
        count: 0
    }

    componentDidMount = () => {
        console.log('mounted (componentDidMount)');
    }

    componentDidUpdate = () => {
        console.log('update (componentDidUpdate)');
    }

    componentWillUpdate = () => {
        console.log('will update (componentWillUpdate)');
    }

    componentWillUnmount = () => {
        console.log('will unmount (componentWillUnmount)');
    }

    handleCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render = () => {
        console.log('RENDER');
        return <h1 onClick={this.handleCount}>LifeCycleClass {this.state.count} {this.props.count}</h1>
    }
}

export default LifeCycleClass;